"use strict";

const {
	src,
	dest,
	series,
	parallel
} = require("gulp"),
	minifycss = require("gulp-clean-css"), //css压缩
	uglify = require("gulp-uglify"), //压缩JS
	concat = require("gulp-concat"), //连接文件，我的理解：将多个文件合并为一个文件
	header = require("gulp-header"), // 添加文件头信息
	buffer = require("vinyl-buffer"), // 缓冲区 
	pkg = require("./package.json"), // 项目信息：package.json
	debug = require("gulp-debug"), // debug 不确定什么作用
	eslint = require("gulp-eslint"), // gulp-eslint**这是一个用于识别和报告在ECMAScript/JavaScript代码中找到的模式的Gulp插件。。
	prettify = require("gulp-jsbeautifier"), //美化代码
	browserify = require("browserify"), // Browserify解决模块化
	source = require("vinyl-source-stream"), //文件流
	rename = require("gulp-rename"); //重命名

const banner = ["/**",
	" * <%= pkg.name %> v<%= pkg.version %>",
	" * Copyright <%= pkg.company %>",
	" * @link <%= pkg.homepage %>",
	" * @license <%= pkg.license %>",
	" */",
	""
].join("\n");

function jsPrettify() {
	return src("./src/js/easymde.js")
		.pipe(prettify({
			js: {
				brace_style: "collapse",
				indent_char: "\t",
				indent_size: 1,
				max_preserve_newlines: 3,
				space_before_conditional: false
			}
		}))
		.pipe(dest("./src/js"));
};


function jsLint() {
	return src("./src/js/easymde.js")
		.pipe(debug())
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
};

function taskBrowserify(opts) {
	return browserify("./src/js/easymde.js", opts)
		.bundle();
}
// browserify:debug" -> lint

function browserifyDebug() {
	return taskBrowserify({
			debug: true,
			standalone: "easymde"
		})
		.pipe(source("easymde.debug.js"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./debug/"));
};

function jsBrowserify() {
	return taskBrowserify({
			standalone: "easymde"
		})
		.pipe(source("easymde.js"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./debug/"));
};

function jsHltOut () {
	const js_files = ["./src/js/highlight-min.js", "./debug/easymde.js"];
	return src(js_files)
		.pipe(concat("easymde.min.js"))
		.pipe(uglify())
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./easymde/"));
};

function jsOut () {
	const js_files = ["./debug/easymde.js"];
	return src(js_files)
		.pipe(concat("easymde.min.js"))
		.pipe(uglify())
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./easymde-supermin/"));
};
//包含Hilight的JS任务
const scriptsTask = series(browserifyDebug,jsBrowserify,jsPrettify,jsLint,jsHltOut)
//不包含Hilight的JS任务
const minScriptsTask = series(browserifyDebug,jsBrowserify,jsPrettify,jsLint,jsOut)

function cssPrettify() {
	return src("./src/css/easymde.css")
		.pipe(prettify({
			css: {
				indentChar: "\t",
				indentSize: 1
			}
		}))
		.pipe(dest("./src/css"));
};


function cssOut1() {
	const css_files = [
		"./node_modules/codemirror/lib/codemirror.css",
		"./src/css/*.css",
		"./node_modules/codemirror-spell-checker/src/css/spell-checker.css"
	];
	return src(css_files)
		.pipe(concat("easymde.css"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./debug/"))
		.pipe(minifycss())
		.pipe(rename("easymde.min.css"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./easymde/"));
};

 function cssOut2() {
	const css_files = [
		"./node_modules/codemirror/lib/codemirror.css",
		"./src/css/easymde.css",
		"./node_modules/codemirror-spell-checker/src/css/spell-checker.css"
	];
	return src(css_files)
		.pipe(concat("easymde.css"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./debug/"))
		.pipe(minifycss())
		.pipe(rename("easymde.min.css"))
		.pipe(buffer())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(dest("./easymde-supermin/"));
};
const minStylesTask = series(cssPrettify,cssOut2)
const stylesTask = series(cssPrettify,cssOut1)

function fontsCopy() {
	return src("./src/fonts/*")
		.pipe(dest("./easymde/fonts"))
		.pipe(dest("./easymde-supermin/fonts"));
};

exports.default = parallel(scriptsTask,minScriptsTask,minStylesTask,stylesTask,fontsCopy)